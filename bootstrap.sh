# System Packages, tools, and dependencies
yes | sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties nodejs imagemagick npm libgdbm-dev libncurses5-dev automake libtool bison libffi-dev make gcc ruby-dev htop qt5-default libqt5webkit5-dev nodejs

# Install RVM/Ruby/Rails
gpg --keyserver hkp://keys.gnupg.net --recv-keys D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable --rails
source /etc/profile.d/rvm.sh

# Install Gems
bundle install

# Setup Database
rake db:create
rake db:migrate
rake db:seed

Rails.application.routes.draw do
  devise_for :users
  resources :pages
  resources :posts
  root :to => 'pages#index'
end
